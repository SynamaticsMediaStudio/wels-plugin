# Wels Live Plugin

The Wels Live Plugin is a WordPress plugin that brings live events, news and share buttons to your website from Facebook API.

## Installation

1. Download the plugin zip file from the [releases page](https://github.com/vishnuraj17/wels-live-plugin/releases).
2. In your WordPress admin dashboard, go to "Plugins" > "Add New".
3. Click on "Upload Plugin" and select the zip file you downloaded.
4. Activate the plugin.

## Usage

### Events API

To display events on your website, add the following shortcode to any page or post:


