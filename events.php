<?php
global $wpdb,$table_name,$plugin_dir;
$get_pms = $wpdb->get_row("SELECT * FROM $table_name");
?>


<div class="items" id="events_list"></div>
<script type="text/javascript">
	jQuery.ajax({
		url: 'https://cors-anywhere.herokuapp.com/https://api.tourdata.at/services/Api/GetEventListProxy.php?ApiKey=<?php echo $get_pms->events_api;?>',
		type: 'GET',
		beforeSend:function(){
			jQuery("#events_list").html("<div class=\"pre_loader_item text-center\"></div>");
		}
	})
	.done(function(res) {
		jQuery("#events_list").html("");
		res = jQuery(res).find("Veranstaltungen Veranstaltung").sort(function(a, b) { // And sort by their study_date elements
		      var d1 = jQuery(a).children('TERMIN_NEXT').text();
		      var d2 = jQuery(b).children('TERMIN_NEXT').text();
		      return (d1 < d2 ? -1 : (d1 > d2 ? +1 : 0));
		});
		jQuery.each(res, function(index, val) {
			moment.updateLocale('de-ch', null);
			$("#events_list").append(
				'<div class="item con-con-row">'+
					'<div class="item-image con-con-col-4" style="background-image:url('+$(val).find("url[size=ltoTeaserBoxImage]").text()+')"></div>'+
					'<div class="item-content con-con-col-8">'+
						'<h5><a data-toggle=\"modal\" data-p-id="'+$(val).attr('VeranstaltungsId')+'" data-target=\"#details-modal\" class=\"details_md\" href=\"#\">'+$(val).children("OBJECT_TEXT_NAME").text()+'</a></h5>'+
						"<small><strong>"+
							"Ort: "+
							$(val).find("Veranstaltungsort NAME").text()+", "+
							$(val).find("Veranstaltungsort STRASSE").text()+", "+
							$(val).find("Veranstaltungsort PLZ").text()+", "+
							$(val).find("Veranstaltungsort ORT").text()+""+
						"</small></strong><br>"+
						"<small><strong> Tag: "+moment(jQuery(val).children("TERMIN_NEXT").text()).format("DD.MM.YYYY - HH:mm")+" bis "+moment(jQuery(val).children("TERMIN_NEXT_ENDE").text().replace(/(<([^>]+)>)/ig,"")).format("HH:mm")+"</strong></small>"+
						
						
						'<p>'+$.trim($(val).children("OBJECT_TEXT_TEASER").text().replace(/(<([^>]+)>)/ig,"")).slice(0, 200)+ "..."+'</p>'+
						
						"<a href=\"#\" data-fb-link=\"<?php echo get_permalink();?>\" data-fb-image=\""+jQuery(val).find("url[size=ltoTeaserBoxImage]").text()+"\"  data-fb-details=\""+$.trim($(val).children("OBJECT_TEXT_TEASER").text()).replace(/(<([^>]+)>)/ig,"")+" on "+moment(jQuery(val).children("TERMIN_NEXT").text()).format("DD.MM.YYYY HH:mm")+"\" data-fb-title=\""+jQuery(val).children("OBJECT_TEXT_NAME").text().replace(/(<([^>]+)>)/ig,"")+"\" class=\"btn btn-sm btn-primary float-right shareBtn\"><i class=\"fa fa-facebook\"></i> Share</a>"+
            "<a class=\"maps-show btn btn-sm btn-danger\" href=\"https://www.google.co.in/maps/search/"+$(val).find("Veranstaltungsort NAME").text()+","+$(val).find("Veranstaltungsort STRASSE").text()+", "+"?hl=en&source=opensearch\" target=\"_blank\"><i class=\"fa fa-map\"></i> auf Karte</a>"+              
						//"<a data-toggle=\"modal\" data-lat=\""+jQuery(val).find("LATITUDE").text()+"\" data-long=\""+jQuery(val).find("LONGITUDE").text()+"\" data-target=\"#map-modal\" class=\"maps-show\" href=\"https://www.google.co.in/maps/search/s?hl=en&source=opensearch\" target=\"_blank\">auf Karte</a>"+							

					'</div>'+
				'</div>'
				)

		});
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		
	});
	//jQuery(val).find("Veranstaltungsort NAME").text()
	
$(document).on('click', '.shareBtn', function(event) {
	event.preventDefault()
	var link 	= $(this).data("fb-link")
		og_img 	= $(this).data("fb-image")
		des 	= $(this).data("fb-details")
		title 	= $(this).data("fb-title")
    FB.ui({
    method: 'share_open_graph',
    action_type: 'og.shares',
    action_properties: JSON.stringify({
        object: {
            'og:url': link,
            'og:title': title,
            'og:description': des,
            'og:image': og_img
        }
    })
},
function (response) {
// Action after response
});
});

</script>
<script>
	// $(document).on('click', '.maps-show', function(event) {
	// 	event.preventDefault();
	// 	var Lat = $(this).data('lat')
	// 	var Long = $(this).data('long')
	// 	initMap(parseInt(Lat),parseInt(Long));
	// });
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '<?php echo $get_pms->facebook_api;?>',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v3.0'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
      var map;
      function initMap(lat,lang) {
      	console.log(lat.toFixed(4))
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: lat, lng: lang},
          zoom: 14
        });
        var marker = new google.maps.Marker({
          position: {lat: lat, lng: lang},
          map: map,
          title: ''
        });
      }
      $(document).on('click', '.details_md', function(event) {
      	event.preventDefault();
      	$("#details-modal .modal-body").html("<div class=\"pre_loader_item text-center\"></div>");
      	var IDD = $(this).data('p-id');
      	$.ajax({
      		url: 'https://cors-anywhere.herokuapp.com/https://api.tourdata.at/services/Api/GetEventDetailsProxy.php?ApiKey=<?php echo $get_pms->events_api;?>&ObjectID='+IDD,
      		type: 'GET',
      		dataType: 'xml',
      	})
      	.done(function(res) {
      		$("#details-modal .modal-body").html("");
      		res = $(res).find('Veranstaltungen Veranstaltung');
      		var telephones = "";
      		var CONTACT = "";
      		var EMAIL = "";
      		var CONTACT_FIRMA = "";
      		var WEBSEITE = "";
      		res.find('TELEFON').each(function(index, val) {
      			if ($(val).text() == 'undefined') {}
      			else{telephones = "<a href=\"tel:"+$(val).text()+"\">"+$(val).text()+'</a><br>'+telephones;}
      		});
      		res.find('CONTACT').each(function(index, val) {
      			if ($(val).text() == 'undefined') {}
      			else{CONTACT = $(val).text()+'<br>'+CONTACT;}
      		});
      		res.find('EMAIL').each(function(index, val) {
      			if ($(val).text() == 'undefined') {}
      			else{EMAIL = "<a href=\"mailto:"+$(val).text()+"\">"+$(val).text()+'</a><br>'+EMAIL;}
      		});
      		res.find('CONTACT_FIRMA').each(function(index, val) {
      			if ($(val).text() == 'undefined') {}
      			else{CONTACT_FIRMA = "<a href=\"mailto:"+$(val).text()+"\">"+$(val).text()+'</a><br>'+CONTACT_FIRMA;}
      		});
      		res.find('WEBSEITE').each(function(index, val) {
      			if ($(val).text() == 'undefined') {}
      			else{WEBSEITE = "<a href=\""+$(val).text()+"\">"+$(val).text()+'</a>,<br>'+WEBSEITE;}
      		});
      		$("#details-modal .modal-body").append("<h2>"+res.children('Bezeichnung').text()+"</h2>")
      		$("#details-modal .modal-body").append("<small><strong>"+moment(jQuery(res).children("TERMIN_NEXT").text()).format("DD.MM.YYYY - HH:mm")+" bis "+moment(jQuery(res).children("TERMIN_NEXT_ENDE").text().replace(/(<([^>]+)>)/ig,"")).format("HH:mm")+"</strong></small><br><br>")
      		$("#details-modal .modal-body").append("<p>"+res.children('OBJECT_TEXT_BESCHREIBUNG').text()+"</p>")
      		$("#details-modal .modal-body").append(
      										"<table class=\"table table-bordered table-responsive\">"+
      											"<tr>"+
      												"<th>Kontakt</th>"+
      												"<td>"+CONTACT+"</td>"+
      											"</tr>"+
      											"<tr>"+
      												"<th>TELEFON</th>"+
      												"<td>"+telephones+"</td>"+
      											"</tr>"+
      											"<tr>"+
      												"<th>EMAIL</th>"+
      												"<td>"+EMAIL+"</td>"+
      											"</tr>"+
      											"<tr>"+
      												"<th>CONTACT FIRMA</th>"+
      												"<td>"+res.find('CONTACT_FIRMA').text()+"</td>"+
      											"</tr>"+
      											"<tr>"+
      												"<th>WEBSEITE</th>"+
      												"<td>"+WEBSEITE+"</td>"+
      											"</tr>"+
      										"</table>")
      	})
      	.fail(function() {
      		console.log("error");
      	})
      	.always(function() {
      		
      	});
      	
      });
</script>
<div class="modal" id="map-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div id="map" class="modal-body">
      </div>
    </div>
  </div>
</div>
<div class="modal" id="details-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo $get_pms->maps_api;?>"></script>