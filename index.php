<?php
/*
Plugin Name: Wels Live Plugin
Plugin URI:  https://wordpress.synamaticsmediastudio.com/
Description: The Plugin Developed for Wels. 
Version:     0.1.2
Author:      Vishnu Raj
Author URI:  https://synamaticsmediastudio.com/
Text Domain: wporg
Domain Path: /languages
License:     GPL2
 
{Plugin Name} is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
{Plugin Name} is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with {Plugin Name}. If not, see {License URI}.
*/
$table_name = $wpdb->prefix . 'wells';
// Pre Install Function 
function pre_install_function(){
   global $wpdb;
   $table_name = $wpdb->prefix . 'wells';
   $sql = "CREATE TABLE $table_name (id mediumint(9) unsigned NOT NULL AUTO_INCREMENT,facebook_api longtext NULL,maps_api longtext NULL,events_api longtext NULL,news_api longtext NULL, PRIMARY KEY  (id));";
   require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
   dbDelta( $sql );
   $upd = $wpdb->insert($table_name,array("id"=>1));
}

function administration_menu_wels() {
    add_menu_page("Systems Administration", "Application", 'manage_options', "wels-admin", "wels_admin","dashicons-screenoptions");
}

function wels_admin(){
   include (__DIR__."/settings.php");
}
function events_api_list_function() {
   $return_string = require __DIR__."/events.php";
}
function news_api_list_function() {
   $return_string = require __DIR__."/news.php";
}
function load_scripts() {
   wp_enqueue_script( 'jquery_2_2_4','https://code.jquery.com/jquery-2.2.4.min.js',"","2.2.4" );
   wp_enqueue_script( 'bootstrap_4_1_1','https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.bundle.min.js',"","4.1.1" );
   wp_enqueue_script( 'momentjs_locale_2_22_2','https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment-with-locales.js',"","2.22.2"  );
   wp_enqueue_script( 'momentjs_locale_2_22_2','https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/locale/de.js',"","2.22.2"  );
   wp_enqueue_style( 'font-awesome','https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css',"","0.0.1"  );
   wp_enqueue_style( 'bootstrap_4_1_1','https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css',"","0.0.1"  );
   wp_enqueue_style( 'base_style_01',plugin_dir_url( __FILE__ ).'/stylesheets.css',"","0.0.1"  );
}
add_action('admin_menu', 'administration_menu_wels');

register_activation_hook( __FILE__, 'pre_install_function' );

add_action('wp_enqueue_scripts', 'load_scripts');
add_shortcode('events_api_list', 'events_api_list_function');
add_shortcode('news_api_list', 'news_api_list_function');
