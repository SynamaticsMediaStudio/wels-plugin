<?php
global $wpdb,$table_name;
?>
<?php 
if (isset($_POST['facebook_api'])) {
	$update_ar =  array();
	foreach ($_POST as $key => $value) {
		$update_ar[$key] = $value;
	}
	$upd = $wpdb->update( $table_name, $update_ar, array("id"=>1));
	if ($upd == '1') {
		echo "<div class=\"updated notice\"><p>Settings Updated</p></div>";
	}
	else{
		echo "<div class=\"error notice\"><p>Plugin couldn't update the settings.</p></div>";		
	}
}
$get_pms = $wpdb->get_row("SELECT * FROM $table_name");
?>
<div class="wrap">
	<h2>System Administration</h2>
	<table class="form-table">
		<form accept="" method="POST" action="">
			<tbody>
				<tr>
					<th scope="row"><label for="facebook_api">Facebook App ID</label></th>
					<td><input name="facebook_api" type="text" id="facebook_api" value="<?php echo $get_pms->facebook_api;?>" class="regular-text"></td>
				</tr>
				<tr>
					<th scope="row"><label for="events_api">Events API</label></th>
					<td><input name="events_api" type="text" id="events_api" value="<?php echo $get_pms->events_api;?>" class="regular-text"></td>
				</tr>
				<tr>
					<th scope="row"><label for="news_api">News API</label></th>
					<td><input name="news_api" type="text" id="news_api" value="<?php echo $get_pms->news_api;?>" class="regular-text"></td>
				</tr>
				<tr>
					<th scope="row"><label for="maps_api">Maps API</label></th>
					<td><input name="maps_api" type="text" id="maps_api" value="<?php echo $get_pms->maps_api;?>" class="regular-text"></td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td scope="row" colspan="2"><button class="button button-primary">Save</button></td>
				</tr>
			</tfoot>
		</form>
	</table>
</div>
